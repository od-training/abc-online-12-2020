import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Video } from './dashboard/models';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  loadVideos() {
    return this.http
      .get<Video[]>('https://api.angularbootcamp.com/videos');
  }
}
