import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { VideoDataService } from 'src/app/video-data.service';
import { Video } from '../models';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss'],
})
export class VideoDashboardComponent implements OnInit {
  selectedVideo: Video | undefined;

  dashboardVideoList: Observable<Video[]>;

  constructor(videoDataService: VideoDataService) {
    this.dashboardVideoList = videoDataService.loadVideos();
  }

  ngOnInit(): void {}

  setSelectedVideo(video: Video) {
    this.selectedVideo = video;
  }
}
