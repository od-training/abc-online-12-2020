import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Video } from '../models';
@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss'],
})
export class VideoListComponent implements OnInit {
  @Input() videoList: Video[] = [];
  @Output() chooseVideo = new EventEmitter<Video>();
  selectedVideo: Video | null = null;

  constructor() {}

  ngOnInit(): void {}

  selectVideo(video: Video) {
    this.chooseVideo.emit(video);
    this.selectedVideo = video;
  }
}
