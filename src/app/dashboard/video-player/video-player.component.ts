import { Component, Input, SimpleChanges } from '@angular/core';
import { Video } from '../models';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { OnChanges } from '@angular/core';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss'],
})
export class VideoPlayerComponent implements OnChanges {
  @Input() video: Video | undefined;
  url: SafeResourceUrl | undefined;
  constructor(private sanitizer: DomSanitizer) {}
  ngOnChanges(changes: SimpleChanges): void {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://www.youtube.com/embed/' + changes.video.currentValue?.id
    );
  }
}
